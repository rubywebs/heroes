﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Message// : ScriptableObject//MonoBehaviour
{
    public int day;
    public int hour;
    public int minute;

    public bool viewed;
    public Color color = Color.black;

    public string msg;
    public string msgType;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}

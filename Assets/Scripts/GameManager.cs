﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //TIME
    [Header("Game Start Time")]
    public int minutes = 0;
    [Tooltip("Hours are based on 24 hour cycle")]
    public int hours = 7;
    public int days = 1;
    [Tooltip("Clock GameObject")]
    public Clock GameClock;
    public Text GameSpeedText;

    [Tooltip("How fast is the game running (seconds / update)?  Slow = 1, Standard = 0.5, Fast = 0.25")]
    public float GameSpeed = 0.5f;  //paused = 0, slow = 1, standard = 0.5, fast = 0.25

    //MANAGERS
    [Header("Managers")]
    public CharacterManager characterManager;

    //LOCATIONS
    [Header("Locations")]
    public List<Location> Locations;
    public GameObject LocationSelectListArea;
    public GameObject LocationSelectPFB;

    //MESSAGES
    [Header("Messages")]
    public List<GameObject> Messages;
    public GameObject messagePFB;
    public GameObject messageList;
    public int maxMessages = 10;

    //PANELS
    [Header("Panels")]
    public GameObject heroPanel;
    public GameObject townPanel;

    //HEROES PANEL
    [Header("Heros Panel")]
    public GameObject heroListArea;
    public GameObject heroListPFB;
    public GameObject heroLocationSelectArea;
    
    //HEROES DETAIL PANEL
    public Text heroName;
    public Text Level;
    public Text ClassName;
    public Text Exp;
    public Text ExpNextLevel;
    public Text HP;
    public Text Strength;
    public Text Speed;
    public Text Dexterity;
    public Text Intelligence;
    public Text Constitution;
    public Text Resilience;

    public Text CurrentMission;
    public Text CurrentTask;
    public Text TaskTime;

    // PREMADE HERO PREFABS
    [Header("Hero Prefabs")]
    public GameObject DefaultHero;

    public Character selectedHero;
    //Delayed mission:  Waiting for location
    public string delayedAction;

    // Use this for initialization
    void Start ()
    {
        //Check for game load
        if (ES2.Exists("GameMaster.bin"))
        {
            //Save game exists, load game
            using (ES2Reader reader = ES2Reader.Create("GameMaster.bin"))
            {
                //Load clock information
                days = reader.Read<int>("days");
                hours = reader.Read<int>("hours");
                minutes = reader.Read<int>("minutes");
                GameSpeed = reader.Read<float>("gameSpeed");

                //Load characters
                characterManager.LoadCharacters();
            }

            //Start the game time

        }
        else
        {
            //Add initial characters
            GameObject nHero = Instantiate(DefaultHero);
            nHero.GetComponent<Character>().cName = "Thor";
            characterManager.AddCharacter(nHero.GetComponent<Character>());

            nHero = Instantiate(DefaultHero);
            nHero.GetComponent<Character>().cName = "Atlas";
            characterManager.AddCharacter(nHero.GetComponent<Character>());

            InvokeRepeating("UpdateGameCycle", 0, GameSpeed);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    
    private void OnApplicationQuit()
    {
        //Save the game
        using (ES2Writer writer = ES2Writer.Create("GameMaster.bin"))
        {
            //Save the game times
            writer.Write(days, "days");
            writer.Write(hours, "hours");
            writer.Write(minutes, "minutes");
            writer.Write(GameSpeed, "gameSpeed");

            
            //Save the characters
            characterManager.SaveCharacters();

            //Finish the save
            writer.Save();
        }
    }

    /// <summary>
    /// Updates the game speed
    /// </summary>
    /// <param name="speed"></param>
    public void UpdateGameSpeed(float speed)
    {
        //Cancel current UpdateGameCycle
        CancelInvoke("UpdateGameCycle");

        if (speed == 0)
            GameSpeedText.text = "Paused";
        else if (speed == 0.1)
            GameSpeedText.text = "Fast";
        else if (speed == 1)
            GameSpeedText.text = "Normal";

        //If not paused, change UpdateGameCycle speed
        if (speed > 0.0f)
            InvokeRepeating("UpdateGameCycle", 0, speed);
    }

    public void ShowHeroesPanel()
    {
        heroPanel.SetActive(true);

        foreach(Transform t in heroListArea.transform)
        {
            GameObject.Destroy(t.gameObject);
        }

        //Populate the heroes list
        foreach(Character c in characterManager.Characters)
        {
            GameObject h = Instantiate(heroListPFB, heroListArea.transform, false);
            h.transform.Find("Text").gameObject.GetComponent<Text>().text = c.cName;
            h.GetComponent<HeroSelection>().selectedHero = c;
        }

        //Show first hero data
        ShowSelectedHeroData(characterManager.Characters[0]);
    }

    public void ShowSelectedHeroData(Character hero)
    {
        //Show hero stats
        heroName.text = hero.cName;
        ClassName.text = hero.cCurrentClass.ClassName;
        Level.text = hero.cLevel.ToString();
        Exp.text = hero.cExperience.ToString();
        ExpNextLevel.text = hero.cExperienceNext.ToString();

        HP.text = string.Format("{0} / {1}", hero.cHpCurrent, hero.cHpMax);
        Strength.text = hero.cStrength.ToString();
        Speed.text = hero.cSpeed.ToString();
        Dexterity.text = hero.cDexterity.ToString();
        Intelligence.text = hero.cIntelligence.ToString();

        //Show hero mission and current task
        CurrentMission.text = hero.currentAction;
        CurrentTask.text = hero.currentTask;
        TaskTime.text = hero.taskTime.ToString();

        //Save the selectedHero for further usage
        selectedHero = hero;
    }

    public void ShowHeroMessages()
    {
        //Clear messages
        Messages.Clear();

        //Clear out the messageList
        foreach(Transform t in messageList.transform)
        {
            GameObject.Destroy(t.gameObject);
        }

        foreach(Message msg in selectedHero.Messages)
        {
            AddNewMessage(selectedHero.cName, msg);
        }
    }



    /// <summary>
    /// Set the nextAction of the selected hero to the new mission.
    /// Selected hero will process the mission.
    /// Some missions require a Location and will wait to process for locaqtion
    /// selection
    /// </summary>
    /// <param name="mission"></param>
    public void SetHeroMission(string mission)
    {
        switch (mission)
        {
            //Gain EXP requires a location
            case "Gain EXP":
                delayedAction = mission;
                break;
            default:
                selectedHero.nextAction = mission;
                break;
        }
    }

    
    public void LoadLocationSelection(bool ShowTowns)
    {
        foreach(Transform t in heroLocationSelectArea.transform)
        {
            GameObject.Destroy(t.gameObject);
        }

        foreach (Location l in Locations)
        {
            if (ShowTowns == false)
            {
                if (l.Town == true)
                    continue;
            }

            GameObject g = Instantiate(LocationSelectPFB, LocationSelectListArea.transform, false);
            g.transform.Find("BtnLocationSelect").transform.Find("Text").gameObject.GetComponent<Text>().text = l.LocationName;
            g.GetComponent<SetLocation>().location = l;
        }
    }

    /// <summary>
    /// Updatse main game logic here.  Only updates when game time is running, not paused
    /// </summary>
    private void UpdateGameCycle()
    {
        GameClock.UpdateTime();

        characterManager.UpdateCharacterActions();

        //If Hero Panel active, refresh
        if (heroPanel.activeSelf)
            ShowSelectedHeroData(selectedHero);
    }

    public void AddNewMessage(string sender, Message msg)
    {
        //Get the message type and get the display sprite
        Sprite s = new Sprite();
        switch (msg.msgType)
        {
            case "level":
                s = characterManager.levelUpImage;
                break;
            case "travel":
                s = characterManager.travelImage;
                break;
            case "combat":
                s = characterManager.combatImage;
                break;
            default:
                break;
        }

        GameObject m = Instantiate(messagePFB, messageList.transform, false);
        //Set the sprite image
        m.transform.Find("Panel").gameObject.transform.Find("MsgType").gameObject.GetComponent<Image>().sprite = s;

        //string msg = string.Format("{0} is currently {1}.", c.cName, c.currentAction);
        m.transform.Find("Panel").gameObject.transform.Find("MessageText").gameObject.GetComponent<Text>().text = msg.msg;
        m.transform.Find("Panel").gameObject.transform.Find("MessageText").gameObject.GetComponent<Text>().color = msg.color;

        m.transform.SetAsFirstSibling();

        Messages.Add(m);
        if (Messages.Count > maxMessages)
        {
            GameObject delete_m = Messages[Messages.Count - 1];
            Messages.RemoveAt(Messages.Count -1);
            GameObject.Destroy(delete_m);
        }
    }
}

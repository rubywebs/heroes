﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSelection : MonoBehaviour
{
    [HideInInspector]
    public Character selectedHero;
    private GameManager gameManager;

	// Use this for initialization
	void Start ()
    {
        //Get the GameManager
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Update Hero Data Panel
    public void UpdateHeroData()
    {
        gameManager.ShowSelectedHeroData(selectedHero);
    }
}

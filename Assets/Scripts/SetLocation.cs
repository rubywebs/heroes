﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLocation : MonoBehaviour 
{
	public Location location;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Set_Location()
	{
		//Get the GameManager
		GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

		gameManager.selectedHero.nextActionLocation = location;
		gameManager.selectedHero.nextAction = gameManager.delayedAction;

		//Disable the hero select panel
		gameManager.heroLocationSelectArea.SetActive(false);
	}
}

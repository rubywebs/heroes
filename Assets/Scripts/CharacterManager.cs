﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterManager : MonoBehaviour
{
    public GameObject messagePFB;
    public GameObject messageList;

    [Header("Message Sprites")]
    public Sprite combatImage;
    public Sprite levelUpImage;
    public Sprite travelImage;
    
    [Header("Managers")]
    public GameManager gameManager;

    public List<Character> Characters;

	// Use this for initialization
	void Start ()
    {
        Characters = new List<Character>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SaveCharacters()
    {
        foreach (Character c in Characters)
        {
            ES2.Save(c.cName, string.Format("Characters/characters.bin?tag={0}", c.cName));
            c.SaveCharacter();
        }
    }

    public void LoadCharacters()
    {
        string[] tags = ES2.GetTags("Characters/characters.bin");

        for(int i = 0; i < tags.Length; i++)
        {
            GameObject c = Instantiate(gameManager.DefaultHero);
            c.name = tags[i];
            c.GetComponent<Character>().LoadCharacter(ES2.Load<string>(string.Format("Characters/characters.bin?tag={0}", tags[i])));

            //Add character into Characters list
            AddCharacter(c.GetComponent<Character>());
        }
    }

    public void AddCharacter(Character character)
    {
        Characters.Add(character);
    }


    /// <summary>
    /// Checks to see if idle characters have a new action
    /// </summary>
    public void UpdateCharacterActions()
    {
        foreach(Character c in Characters)
        {
            if (c.currentAction == "Idle" && c.idleTimer-- <= 0)
            {
                c.CheckForNewAction();

                //string msg = string.Format("{0} is currently {1}.", c.cName, c.currentAction);
                //gameManager.AddNewMessage(c.cName, msg);
            }

            //Check for the next task
            if (c.currentAction != "Idle")
                c.ProcessTask();
        }
    }
}

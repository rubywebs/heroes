﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterClass : MonoBehaviour 
{
	public string ClassName;
	public int MinHp;
	public int MaxHp;
	public int MinStrength;
	public int MaxStrength;
	public int MinIntelligence;
	public int MaxIntelligence;
	public int MinSpeed;
	public int MaxSpeed;
	public int MinDexterity;
	public int MaxDexterity;
    public int MinConstitution;
    public int MaxConstitution;
    public int MinResilience;
    public int MaxResilience;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}

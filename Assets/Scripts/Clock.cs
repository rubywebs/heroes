﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour {
    public Text clockText;
    public GameManager gameManager;

    
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateTime()
    {
        //Add 1 minute
        gameManager.minutes++;
        
        //if minutes > 59, create a new hour
        if(gameManager.minutes > 59)
        {
            gameManager.minutes = 0;
            gameManager.hours++;

            //if hours > 23, create a new day
            if(gameManager.hours > 23)
            {
                gameManager.days++;
                gameManager.hours = 0;
            }
        }

        //Update the time clock
        clockText.text = string.Format("Day {0}: {1:00}:{2:00}     ", gameManager.days, gameManager.hours, gameManager.minutes);
    }
}

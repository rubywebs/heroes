﻿using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public GameManager gameManager;

    [Header("Attributes")]
    public string cName;
    public int cLevel;
    public int cExperience;
    public int cExperienceNext;
    public int cHpMax;
    public int cHpCurrent;
    public int cStrength;  //Damage
    public int cIntelligence;  //Magic damage
    public int cSpeed;  //Action speed
    public int cDexterity;  //Hand speed, IE bow, dagger, etc
    public int cConstitution;  //Physical resistance
    public int cResilience;  //Magical resistance

    public List<CharacterClass> cClasses;
    public CharacterClass cCurrentClass;

    //How often to check for new action while idle
    [HideInInspector]
    public int idleTimer = 0;

    //Actions: "Idle"
    public string currentAction = "Idle";
    public Location currentActionLocation;
    public string nextAction = "Idle";
    public Location nextActionLocation;

    [HideInInspector]
    public string currentTask;
    public string nextTask;
    public int taskStage = 0;
    public int taskTime = 0;

    public int encounterTime = 0;  //How long until next encounter?  Can be modified by class, action, etc

    [Header("Messages")]
    //MESSAGES
    public List<Message> Messages;

    // Use this for initialization
    void Start ()
    {
        //Calculate next level experience
        if (cExperienceNext == 0)
        {
            float nextExp = Mathf.Round((cLevel + 125 * Mathf.Pow(2, (float)cLevel / 7.0f)) / 4.0f);
            cExperienceNext = (int)nextExp;
        }
        //cExperienceNext = 10;

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        //Initialize Messages list
        Messages = new List<Message>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    /// <summary>
    /// Save the character to file: CharacterName
    /// </summary>
    public void SaveCharacter()
    {
        using (ES2Writer writer = ES2Writer.Create(string.Format("Characters/{0}.bin", cName)))
        {
            writer.Write(cName, "name");

            //Write stats
            writer.Write(cLevel, "level");
            writer.Write(cExperience, "exp");
            writer.Write(cExperienceNext, "nextExp");
            writer.Write(cHpCurrent, "hpCurrent");
            writer.Write(cHpMax, "hpMax");
            writer.Write(cStrength, "strength");
            writer.Write(cSpeed, "speed");
            writer.Write(cIntelligence, "intelligence");
            writer.Write(cDexterity, "dexterity");
            writer.Write(cConstitution, "constitution");
            writer.Write(cResilience, "resilience");

            //Current class
            writer.Write(cCurrentClass.ClassName, "currentClass");

            //Actions
            writer.Write(idleTimer, "idleTimer");
            writer.Write(currentAction, "currentAction");
            writer.Write(nextAction, "nextAction");

            writer.Write(currentTask, "currentTask");
            writer.Write(nextTask, "nextTask");

            writer.Write(taskStage, "taskStage");
            writer.Write(taskTime, "taskTime");

            writer.Write(encounterTime, "encounterTime");

            writer.Write(Messages.Count, "msgCount");

            using (ES2Writer msgwriter = ES2Writer.Create(string.Format("Characters/{0}_msgs.bin", cName)))
            {
                int i = 0;
                foreach (Message m in Messages)
                {
                    msgwriter.Write(m.msgType, string.Format("{0}_msgType", i));
                    msgwriter.Write(m.msg, string.Format("{0}_msg", i));
                    i++;
                }

                msgwriter.Save();
            }

            //Finish the save
            writer.Save();
        }
    }

    /// <summary>
    /// Load the character data back in
    /// </summary>
    /// <param name="file"></param>
    public void LoadCharacter(string file)
    {
        using (ES2Reader reader = ES2Reader.Create(string.Format("Characters/{0}.bin", file)))
        {
            cName = reader.Read<string>("name");

            //Load stats
            cLevel = reader.Read<int>("level");
            cExperience = reader.Read<int>("exp");
            cExperienceNext = reader.Read<int>("nextExp");
            cHpCurrent = reader.Read<int>("hpCurrent");
            cHpMax = reader.Read<int>("hpMax");
            cStrength = reader.Read<int>("strength");
            cSpeed = reader.Read<int>("speed");
            cIntelligence = reader.Read<int>("intelligence");
            cDexterity = reader.Read<int>("dexterity");
            if (reader.TagExists("constitution")) cConstitution = reader.Read<int>("constitution");
            if (reader.TagExists("resilience")) cResilience = reader.Read<int>("resilience");

            idleTimer = reader.Read<int>("idleTimer");
            currentAction = reader.Read<string>("currentAction");
            nextAction = reader.Read<string>("nextAction");

            currentTask = reader.Read<string>("currentTask");
            nextTask = reader.Read<string>("nextTask");

            taskStage = reader.Read<int>("taskStage");
            taskTime = reader.Read<int>("taskTime");

            encounterTime = reader.Read<int>("encounterTime");

            //Find current class
            foreach (CharacterClass c in cClasses)
            {
                if (c.ClassName == reader.Read<string>("currentClass"))
                    cCurrentClass = c;
            }

            int count = reader.Read<int>("msgCount");
            if (count > 0)
            {
                using (ES2Reader msgreader = ES2Reader.Create(string.Format("Characters/{0}.bin", file)))
                {
                    for (int i = 0; i < count; i++)
                    {
                        Message m = new Message()
                        {
                            msgType = msgreader.Read<string>(string.Format("{0}_msgType", i)),
                            msg = msgreader.Read<string>(string.Format("{0}_msg", i))
                        };
                    }
                }
            }
        }
    }

    public void AddExperience(int Exp)
    {
        cExperience += Exp;

        if(cExperience > cExperienceNext)
        {
            //Level UP!


            //TODO:  Increase attributes here based off of class
            cLevel++;
            cHpMax += UnityEngine.Random.Range(cCurrentClass.MinHp, cCurrentClass.MaxHp);
            cStrength += UnityEngine.Random.Range(cCurrentClass.MinStrength, cCurrentClass.MaxStrength);
            cIntelligence += UnityEngine.Random.Range(cCurrentClass.MinIntelligence, cCurrentClass.MaxIntelligence);
            cSpeed += UnityEngine.Random.Range(cCurrentClass.MinSpeed, cCurrentClass.MaxSpeed);
            cDexterity += UnityEngine.Random.Range(cCurrentClass.MinDexterity, cCurrentClass.MaxDexterity);

            //Calulate next cExperienceNext
            float nextExp = cExperienceNext + Mathf.Round(cLevel + 125 * Mathf.Pow(2, ((float)cLevel / 7.0f))) / 4.0f;
            cExperienceNext = (int)nextExp;

            Message m = new Message()
            {
                msg = "I have leveled up!",
                day = gameManager.days,
                hour = gameManager.hours,
                minute = gameManager.minutes,
                msgType = "level"
            };

            //Add the message
            Messages.Add(m);

            //Refresh message list
            //gameManager.ShowHeroMessages();

        }
    }

    /// <summary>
    /// Checks to see if there is a new action.
    /// If the nextAction is an action, set the first nextTask to Prepare
    /// </summary>
    public void CheckForNewAction()
    {
        currentAction = nextAction;
        nextAction = "Idle";

        switch (currentAction)
        {
            case "Idle":
                idleTimer = 15;
                break;
            case "Gain EXP":
                currentTask = "";
                nextTask = "Prepare";
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Has the task time completed?  If so, move to next task in the action
    /// </summary>
    public void ProcessTask()
    {
        //If there is no current task OR the current task time <= 0
        if (taskStage == 0 || taskTime-- <= 0)
        {
            taskStage++;

            switch (currentAction)
            {
                case "Gain EXP":
                    switch (taskStage)
                    {
                        case 1:
                            currentTask = "Preparing";
                            nextTask = "Travel";
                            taskTime = 15;
                            break;
                        case 2:
                            currentTask = "Traveling";
                            nextTask = "Gain EXP";
                            taskTime = 30;  //TODO taskTime = distance traveled
                            break;
                        case 3:
                            //Update locations
                            currentActionLocation = nextActionLocation;
                            nextActionLocation = null;
                            currentTask = "Looking for enemies";
                            nextTask = "Returning";
                            taskTime = 240;  //TODO taskTime = Time Remain - travel time before sundown

                            //Set encounter time.  Default is 1 / hour.
                            encounterTime = 60;
                            break;
                        case 4:
                            currentTask = "Returning";
                            nextTask = "";
                            taskTime = 30;
                            break;
                        case 5:
                            currentAction = "Idle";
                            currentTask = "";
                            taskStage = 0;
                            break;
                        default:
                            break;
                    }
                    break;
                    default:
                        break;
                    
            }

            //Reset taskTime if < 0
            if (taskTime < 0) taskTime = 0;

            return;
        }

        switch (currentTask)
        {
            case "Preparing":
                //Current task is Prepare
                //TODO: What happens during prepare?  For now, do nothing
                break;
            case "Traveling":
                //Current task is travel
                //TODO:  Can hero be attacked?  For now, do nothing
                break;
            case "Looking for enemies":
                //Current task is Gain EXP.  Default is 1 / hour
                //Hero is looking for trouble, so decrease encounter time faster

                if(encounterTime-- <= 0)
                {
                    //Random Encounter
                    //TODO: Initiate battle.  For now, just add EXP and reset encounterTime

                    //Display a new message
                    Message m = new Message()
                    {
                        day = gameManager.days,
                        hour = gameManager.hours,
                        minute = gameManager.minutes,
                        msg = "I have cleared a random encounter and earned 10 EXP!",
                        msgType = "combat"
                    };

                    //Add the message
                    Messages.Add(m);

                    //Refresh message list
                    //gameManager.ShowHeroMessages();

                    AddExperience(10);
                    encounterTime = 60;
                }
                break;
            default:
                break;
        }
    }

    
}

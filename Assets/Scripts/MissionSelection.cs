﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionSelection : MonoBehaviour
{
    public GameManager gameManager;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    /// <summary>
    /// Send the mission to gameManager to set the hero mission
    /// </summary>
    /// <param name="mission"></param>
    public void SelectMission(string mission)
    {
        gameManager.SetHeroMission(mission);
    }

    
}

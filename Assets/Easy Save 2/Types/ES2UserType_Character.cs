using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_Character : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		Character data = (Character)obj;
		// Add your writer.Write calls here.
writer.Write(data.cLevel);writer.Write(data.cExperience);writer.Write(data.cExperienceNext);writer.Write(data.cHpMax);writer.Write(data.cHpCurrent);writer.Write(data.cStrength);writer.Write(data.cIntelligence);writer.Write(data.cSpeed);writer.Write(data.cDexterity);writer.Write(data.idleTimer);writer.Write(data.taskStage);writer.Write(data.taskTime);writer.Write(data.encounterTime);writer.Write(data.useGUILayout);
writer.Write(data.enabled);
writer.Write(data.hideFlags);

	}
	
	public override object Read(ES2Reader reader)
	{
		Character data = GetOrCreate<Character>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		Character data = (Character)c;
		// Add your reader.Read calls here to read the data into the object.
data.cLevel = reader.Read<System.Int32>();
data.cExperience = reader.Read<System.Int32>();
data.cExperienceNext = reader.Read<System.Int32>();
data.cHpMax = reader.Read<System.Int32>();
data.cHpCurrent = reader.Read<System.Int32>();
data.cStrength = reader.Read<System.Int32>();
data.cIntelligence = reader.Read<System.Int32>();
data.cSpeed = reader.Read<System.Int32>();
data.cDexterity = reader.Read<System.Int32>();
data.idleTimer = reader.Read<System.Int32>();
data.taskStage = reader.Read<System.Int32>();
data.taskTime = reader.Read<System.Int32>();
data.encounterTime = reader.Read<System.Int32>();
data.useGUILayout = reader.Read<System.Boolean>();
data.enabled = reader.Read<System.Boolean>();
data.hideFlags = reader.Read<UnityEngine.HideFlags>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_Character():base(typeof(Character)){}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_GameManager : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		GameManager data = (GameManager)obj;
		// Add your writer.Write calls here.
writer.Write(data.minutes);writer.Write(data.hours);writer.Write(data.days);writer.Write(data.GameSpeed);writer.Write(data.maxMessages);writer.Write(data.useGUILayout);
writer.Write(data.enabled);
writer.Write(data.hideFlags);

	}
	
	public override object Read(ES2Reader reader)
	{
		GameManager data = GetOrCreate<GameManager>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		GameManager data = (GameManager)c;
		// Add your reader.Read calls here to read the data into the object.
data.minutes = reader.Read<System.Int32>();
data.hours = reader.Read<System.Int32>();
data.days = reader.Read<System.Int32>();
data.GameSpeed = reader.Read<System.Single>();
data.maxMessages = reader.Read<System.Int32>();
data.useGUILayout = reader.Read<System.Boolean>();
data.enabled = reader.Read<System.Boolean>();
data.hideFlags = reader.Read<UnityEngine.HideFlags>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_GameManager():base(typeof(GameManager)){}
}